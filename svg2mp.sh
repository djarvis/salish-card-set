#!/bin/bash

for i in *.svg; do
  BASENAME=$(basename $i .svg)
  PDF=${BASENAME}.pdf
  METAPOST=${BASENAME}.mp

  echo "Converting $i to $METAPOST ..."
  rsvg-convert $i -f pdf -o $PDF
  pstoedit -q $PDF -f mpost $METAPOST
done

