# Overview

This software provides a way to create a custom set of cards that can be used
to play the [SET Card Game](http://www.setgame.com/set).

# Requirements

You will need the following software packages:

* Linux - Operating system
* [ConTeXt](http://wiki.contextgarden.net) - Typesetting engine
* [Inkscape](http://www.inkscape.org) - Vector drawing software
* rsvg-convert - Converts SVG files to PDF
* [pstoedit](http://www.pstoedit.net) - Converts PDF files to MetaPost

# Custom Shape

Use the svg2mp.sh shell script to convert the SVG files:

1. [Install ConTeXt](http://wiki.contextgarden.net/Installation).
1. Create or edit the art in Inkscape (the simpler the better).
1. Convert the SVG to [MetaPost](http://en.wikipedia.org/wiki/MetaPost).
1. Copy the MetaPost shape information from the .mp file.
1. Paste over the shape information inside cards.tex.
1. Run: context cards.tex to produce a PDF.

For example:

1. Open a terminal.
1. Change to the template directory.
1. inkscape trigon.svg
1. Edit as required.
1. Save trigon.svg.
1. Open a terminal.
1. Run: ../svg2mp.sh
1. Edit trigon.mp
1. Copy the draw command.
1. Edit ../cards.tex
1. Find the shape variable assignment.
1. Replace the variable assignment with the corresponding code from trigon.mp.
1. Save ../cards.tex
1. Change to the parent directory (where cards.tex is located).
1. Run: context cards.tex

This will produce a PDF with the new shape.

